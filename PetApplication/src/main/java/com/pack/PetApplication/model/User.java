
    
package com.pack.PetApplication.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="user1")
public class User {​​​​​​​


    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)    
    private int userId;
    private String userName;
    private Long phoneNumber;
    private String Email;
    private String password;
    public int getUserId() {​​​​​​​
        return userId;
    }​​​​​​​
    public void setUserId(int userId) {​​​​​​​
        this.userId = userId;
    }​​​​​​​
    public String getUserName() {​​​​​​​
        return userName;
    }​​​​​​​
    public void setUserName(String userName) {​​​​​​​
        this.userName = userName;
    }​​​​​​​
    public Long getPhoneNumber() {​​​​​​​
        return phoneNumber;
    }​​​​​​​
    public void setPhoneNumber(Long phoneNumber) {​​​​​​​
        this.phoneNumber = phoneNumber;
    }​​​​​​​
    public String getEmail() {​​​​​​​
        return Email;
    }​​​​​​​
    public void setEmail(String email) {​​​​​​​
        Email = email;
    }​​​​​​​
    public String getPassword() {​​​​​​​
        return password;
    }​​​​​​​
    public void setPassword(String password) {​​​​​​​
        this.password = password;
    }​​​​​​​
    public User(int userId, String userName, Long phoneNumber, String email, String password) {​​​​​​​
        super();
        this.userId = userId;
        this.userName = userName;
        this.phoneNumber = phoneNumber;
        Email = email;
        this.password = password;
    }​​​​​​​
    public User() {​​​​​​​
        super();
        // TODO Auto-generated constructor stub
    }​​​​​​​
    @Override
    public String toString() {​​​​​​​
        return "User [userId=" + userId + ", userName=" + userName + ", phoneNumber=" + phoneNumber + ", Email=" + Email
                + ", password=" + password + "]";
    }​​​​​​​
}​​​​​​​
 





