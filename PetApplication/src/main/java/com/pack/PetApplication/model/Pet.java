package com.pack.PetApplication.model;

 

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

 

@Entity
@Table(name="pet1")
public class Pet {

 

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int PetId;
    private String PetName;
    private String PetBreed;
    private String PetColour;
    private int PetAge;
    public int getPetId() {
        return PetId;
    }
    public void setPetId(int petId) {
        PetId = petId;
    }
    public String getPetName() {
        return PetName;
    }
    public void setPetName(String petName) {
        PetName = petName;
    }
    public String getPetBreed() {
        return PetBreed;
    }
    public void setPetBreed(String petBreed) {
        PetBreed = petBreed;
    }
    public String getPetColour() {
        return PetColour;
    }
    public void setPetColour(String petColour) {
        PetColour = petColour;
    }
    public int getPetAge() {
        return PetAge;
    }
    public void setPetAge(int petAge) {
        PetAge = petAge;
    }
    public Pet(int petId, String petName, String petBreed, String petColour, int petAge) {
        super();
        PetId = petId;
        PetName = petName;
        PetBreed = petBreed;
        PetColour = petColour;
        PetAge = petAge;
    }
    public Pet() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Override
    public String toString() {
        return "Pet [PetId=" + PetId + ", PetName=" + PetName + ", PetBreed=" + PetBreed + ", PetColour=" + PetColour
                + ", PetAge=" + PetAge + "]";
    }
}