package com.pack.PetApplication.controller;

 

 

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

 

import javax.validation.Valid;

 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

 

import com.pack.PetApplication.dao.PetRepository;
import com.pack.PetApplication.dao.UserRepository;
import com.pack.PetApplication.exceptions.ResourceNotFoundException;
import com.pack.PetApplication.model.Pet;

 


@RestController @CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class PetController {
    @Autowired 
    UserRepository userRepository;

 

    @Autowired
    PetRepository petRepository;

 

    @GetMapping("/pets")
    public List<Pet> getAllPet() {
        return petRepository.findAll();
    }

 


    @PostMapping("/register")
    public Pet createPet(@Valid @RequestBody Pet pet) {
        return petRepository.save(pet);
    }

 

    @PutMapping("/pets")
    public ResponseEntity<Pet> updatePet(@PathVariable(value = "id") Long petId,
            @Valid @RequestBody Pet petDetails) throws ResourceNotFoundException {
        Pet pet = petRepository.findById(petId)
                .orElseThrow(() -> new ResourceNotFoundException("Pet not found for this id :: " + petId));
        return null;

 

    }

 

    @DeleteMapping("/pets")
    public Map<String, Boolean> deletePet(@PathVariable(value = "id") Long petId)
            throws ResourceNotFoundException {
        Pet pet = petRepository.findById(petId)
                .orElseThrow(() -> new ResourceNotFoundException("Pet not found for this id :: " + petId));

 

        petRepository.delete(pet);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}