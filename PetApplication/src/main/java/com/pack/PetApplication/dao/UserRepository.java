package com.pack.PetApplication.dao;

 

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

 

import com.pack.PetApplication.model.User;

 

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

 

}